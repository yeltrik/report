<div class="dropdown-menu" aria-labelledby="yeltrik-report">

{{--    @includeIf('report::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.index')--}}
{{--    <div class="dropdown-divider"></div>--}}

    @includeIf('mediasite::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.report')
    @includeIf('profile::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.report')
    @includeIf('pdPSR::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.report')
    @includeIf('teachingHonors::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.report')
    @includeIf('transcription::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.report')

</div>
