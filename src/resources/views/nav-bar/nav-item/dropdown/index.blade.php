@can('report')
    <li class="nav-item dropdown">
        @include('report::nav-bar.nav-item.dropdown.nav-link.index')
        @include('report::nav-bar.nav-item.dropdown.dropdown-menu.index')
    </li>
@endcan
